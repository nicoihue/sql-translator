import mongoToSqlTranslator from '../../src';

describe('mongoToSqlTranslator', () => {
  it('translates complex MongoDB query to SQL', () => {
    const mongodbQuery = `db.users.find({
      $or: [
        { age: { $gte: 18 } },
        {
          $and: [
            { gender: 'female' },
            { age: { $gte: 25, $lt: 30 } },
          ],
        },
      ],
    })`;

    const sqlTranslation: string = mongoToSqlTranslator(mongodbQuery);

    expect(sqlTranslation).toBe(
      'SELECT * FROM users WHERE (age >= 18 OR (gender = "female" AND age >= 25 AND age < 30));'
    );
  });

  it('translates another complex MongoDB query to SQL', () => {
    const anotherMongodbQuery = `db.users.find({
      $and: [
        { name: 'John' },
        {
          $or: [
            { age: { $gte: 25 } },
            {
              $and: [
                { gender: 'male' },
                { hobbies: { $in: ['reading', 'coding'] } },
              ],
            },
          ],
        },
      ],
    })`;

    const anotherSqlTranslation: string =
      mongoToSqlTranslator(anotherMongodbQuery);

    expect(anotherSqlTranslation).toBe(
      'SELECT * FROM users WHERE (name = "John" AND (age >= 25 OR (gender = "male" AND hobbies IN ("reading", "coding"))));'
    );
  });

  it('translates simple MongoDB query to SQL', () => {
    const simpleone = `db.users.find({ name: 'John' })`;
    const simpleoneSqlTranslation: string = mongoToSqlTranslator(simpleone);

    expect(simpleoneSqlTranslation).toBe(
      'SELECT * FROM users WHERE name = "John";'
    );
  });
});
