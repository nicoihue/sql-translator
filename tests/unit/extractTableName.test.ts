import extractTableName from '../../src/extractTableName';

describe('extractTableName', () => {
  it('should extract table name from a valid MongoDB query string', () => {
    const queryString = 'db.users.find({ name: "John" })';
    const tableName = extractTableName(queryString);
    expect(tableName).toBe('users');
  });

  it('should throw an error for an invalid MongoDB query string', () => {
    const invalidQueryString = 'invalid_string';
    expect(() => extractTableName(invalidQueryString)).toThrow('Error splitting mongo query string into an array of strings');
  });
});
