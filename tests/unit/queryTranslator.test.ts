import { MongoDBQuery } from '../../src/interfaces/mongoDBQuery';
import queryTranslator from '../../src/queryTranslator';

describe('queryTranslator', () => {
  it('should translate a simple MongoDB query to SQL', () => {
    const mongoDBQuery: MongoDBQuery = { name: 'John' };
    const translatedQuery = queryTranslator(mongoDBQuery);

    expect(translatedQuery).toBe('name = "John"');
  });

  it('should handle $and and $or operators in MongoDB query', () => {
    const mongoDBQuery: MongoDBQuery = {
      $or: [
        { age: { $gte: 25 } },
        {
          $and: [
            { gender: 'male' },
            { hobbies: { $in: ['reading', 'coding'] } },
          ],
        },
      ],
    };
    const translatedQuery = queryTranslator(mongoDBQuery);

    expect(translatedQuery).toBe(
      '(age >= 25 OR (gender = "male" AND hobbies IN ("reading", "coding")))'
    );
  });

  it('should handle $in operator with SQL format', () => {
    const mongoDBQuery: MongoDBQuery = {
      hobbies: { $in: ['reading', 'coding'] },
    };
    const translatedQuery = queryTranslator(mongoDBQuery);

    expect(translatedQuery).toBe('hobbies IN ("reading", "coding")');
  });

  it('should handle specific operators like $gte, $lt', () => {
    const mongoDBQuery: MongoDBQuery = {
      age: { $gte: 25, $lt: 30 },
    };
    const translatedQuery = queryTranslator(mongoDBQuery);

    expect(translatedQuery).toBe('age >= 25 AND age < 30');
  });
});
