import parseMongoDBQueryString from '../../src/parseMongoDBQueryString';

describe('parseMongoDBQueryString', () => {
  it('should parse a simple MongoDB query string', () => {
    const queryString = 'db.users.find({ name: "John" })';
    const parsedQuery = parseMongoDBQueryString(queryString);

    expect(parsedQuery).toEqual({
      name: 'John',
    });
  });

  it('should parse a MongoDB query string with $and and $or operators', () => {
    const queryString =
      'db.users.find({ $and: [ { name: "John" }, { $or: [ { age: { $gte: 25 } }, { gender: "male" } ] } ] })';
    const parsedQuery = parseMongoDBQueryString(queryString);

    expect(parsedQuery).toEqual({
      $and: [
        { name: 'John' },
        {
          $or: [{ age: { $gte: 25 } }, { gender: 'male' }],
        },
      ],
    });
  });

  it('should handle $in operator', () => {
    const queryString =
      'db.users.find({ hobbies: { $in: ["reading", "coding"] } })';
    const parsedQuery = parseMongoDBQueryString(queryString);

    expect(parsedQuery).toEqual({
      hobbies: { $in: ['reading', 'coding'] },
    });
  });

  it('should throw an error for an invalid MongoDB query string', () => {
    const invalidQueryString = 'invalid_string';
    expect(() => parseMongoDBQueryString(invalidQueryString)).toThrow(
      'Error parsing MongoDB query string:'
    );
  });
});
