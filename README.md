# MongoDB to SQL Translator

This project provides a MongoDB to SQL translator utility. It allows you to convert MongoDB-style queries into SQL queries.

## Prerequisites

- Node.js (v14 or higher)
- npm (Node Package Manager)

## Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/nicoihue/sql-translator

   ```

2. Navigate to the project directory:

3. Install dependencies:

   ```bash
   npm install
   ```

## Execute the Translator

1. Run the translator in development mode:

   ```bash
   npm start:dev

   ```

2. Build the project:

   ```bash
   npm run build

   ```

3. Run tests:

   ```bash
   npm run test







   ```
