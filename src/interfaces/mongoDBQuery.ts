export interface MongoDBQuery {
  [key: string]: any;
}
