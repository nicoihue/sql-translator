const extractTableName = (queryString: string): string => {
  const splitted = queryString.split('.');
  if (splitted.length === 1) {
    throw new Error(
      'Error splitting mongo query string into an array of strings'
    );
  }
  return splitted[1];
};

export default extractTableName;
