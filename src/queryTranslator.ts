import { MongoDBQuery } from './interfaces/mongoDBQuery';

const queryTranslator = (query: MongoDBQuery): string => {
  const operatorMapping: { [key: string]: string } = {
    $or: 'OR',
    $and: 'AND',
    $lt: '<',
    $lte: '<=',
    $gt: '>',
    $gte: '>=',
    $ne: '!=',
    $in: 'IN',
  };

  const sqlQuery: string[] = [];

  for (const key in query) {
    if (key in operatorMapping) {
      if (key === '$in') {
        // Handle $in operator
        const field = Object.keys(query[key])[0];
        const values = query[key][field];
        sqlQuery.push(
          `${field} ${operatorMapping[key]} (${values
            .map((val: any) => JSON.stringify(val))
            .join(', ')})`
        );
      } else {
        // Handle other operators
        const subQueries = query[key].map((subQuery: MongoDBQuery) =>
          queryTranslator(subQuery)
        );
        sqlQuery.push(`(${subQueries.join(` ${operatorMapping[key]} `)})`);
      }
    } else {
      // Handle field-value comparison
      if (typeof query[key] === 'object' && !Array.isArray(query[key])) {
        // Handle specific operators like $gte, $lt, $in
        const field = key;
        const operators = Object.keys(query[key]);
        const conditions = operators.map((operator) => {
          const value = query[key][operator];
          if (operator === '$in') {
            // Handle $in operator with sql format
            return `${field} ${operatorMapping[operator]} (${value
              .map((val: any) => JSON.stringify(val))
              .join(', ')})`;
          } else {
            return `${field} ${operatorMapping[operator]} ${JSON.stringify(
              value
            )}`;
          }
        });
        sqlQuery.push(conditions.join(' AND '));
      } else {
        // Handle field-value comparison without operators
        sqlQuery.push(`${key} = ${JSON.stringify(query[key])}`);
      }
    }
  }

  return sqlQuery.join(' ');
};

export default queryTranslator;
