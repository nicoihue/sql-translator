import { MongoDBQuery } from './interfaces/mongoDBQuery';

const parseMongoDBQueryString = (queryString: string): MongoDBQuery => {
  try {
    const trimmedString = queryString.split('find')[1].split(' ').join('');

    const queryObject = eval(`(${trimmedString})`);

    return queryObject;
  } catch (error) {
    throw new Error('Error parsing MongoDB query string:');
  }
};

export default parseMongoDBQueryString;
