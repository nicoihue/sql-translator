import extractTableName from './extractTableName';
import parseMongoDBQueryString from './parseMongoDBQueryString';
import queryTranslator from './queryTranslator';

const mongoToSqlTranslator = (mongoQuery: string) => {
  let sql: string;
  try {
    const tableName = extractTableName(mongoQuery);
    sql = `SELECT * FROM ${tableName}`;
    const queryString = parseMongoDBQueryString(mongoQuery);
    if (queryString) {
      const query = queryTranslator(queryString);
      sql += ` WHERE ${query};`;
    } else {
      sql += ';';
    }
  } catch (error) {
    console.error(error);
    throw error;
  }

  return sql;
};

export default mongoToSqlTranslator;
